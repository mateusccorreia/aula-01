
const api = axios.create({
    baseURL: "https://api.escuelajs.co/api/v1"
})


const findProducts = async () => {
    const { data } = await api.get("/products")
    return data
}

const appendElements = async () => {
    const section = document.querySelector("section")

    const data = await findProducts()

    data.forEach((p, k) => {

        const card = document.createElement('div')
        const img = document.createElement("img")
        const title = document.createElement("span")
        const price = document.createElement("span")
        const description = document.createElement("p")
        const button = document.createElement("button")

        card.className = "card"

        title.innerHTML = p.title
        title.style.margin = "10px"
        price.innerHTML = `R$ ${p.price},00`
        description.innerHTML = p.description
        description.style.margin = "10px"

        button.className = "btn-success"
        button.innerHTML = "Adicionar ao Carrinho"

        img.src = p.images[0]
        img.width = 280

        card.append(img)
        card.append(title)
        card.append(price)
        card.append(description)
        card.append(button)

        section.appendChild(card)

    })
}


window.onload = () => {
    appendElements()
}